import React from "react";
import Translate from "react-translate-component";
import {ChainStore} from "bitsharesjs/es";
import ChainTypes from "components/Utility/ChainTypes";
import BindToChainState from "components/Utility/BindToChainState";
import WithdrawModalBlocktrades from "./WithdrawModalBlocktrades";
import BaseModal from "../../Modal/BaseModal";
import ZfApi from "react-foundation-apps/src/utils/foundation-api";
import AccountBalance from "../../Account/AccountBalance";
import BlockTradesDepositAddressCache from "common/BlockTradesDepositAddressCache";
import AssetName from "components/Utility/AssetName";
import LinkToAccountById from "components/Utility/LinkToAccountById";
import {
    requestDepositAddress,
    getDepositAddress
} from "common/blockTradesMethods";
import {blockTradesAPIs} from "api/apiConfig";
import LoadingIndicator from "components/LoadingIndicator";
import counterpart from "counterpart";

let copyText = "";

class BlockTradesGatewayDepositRequest extends React.Component {
    static propTypes = {
        action: React.PropTypes.string
    };

    static defaultProps = {
        autosubscribe: false
    };

    constructor(props) {
        super(props);

        this.state = {
            url: props.url,
            clipboardText: "",
            loading: false
        };
        this._copy = this._copy.bind(this);
    }

    _copy(e) {
        let that = this;
        copyText = that.state.clipboardText;
        try {
            e.preventDefault();
            e.clipboardData.setData("text/plain", copyText);
        } catch (err) {
            console.error(err);
        }
    }

    componentDidMount() {
        document.addEventListener("copy", this._copy);
        this.getAddress();
    }

    componentWillUnmount() {
        document.removeEventListener("copy", this._copy);
    }

    onWithdraw() {
        ZfApi.publish("withdraw_bdx_modal", "open");
    }

    getAddress() {
        // showing loader
        this.setState({loading: true});
        let accountId = this.props.account.get("name");
        //return;
        fetch(
            "siteapi/exploreruser?username=" + accountId
        
            , {
                method: "GET",
                headers: {
                    Accept: "*/*",
                    "Access-Control-Allow-Origin":"*"
                },
            })
            .then(Response => {
                console.log(JSON.stringify(Response))
                Response.json();
            })
            .then(response => {
                //hiding loader
                this.setState({
                    loading: false,
                    clipboardText: response.address
                });
                console.log(response);
            })
            .catch(err => {
                this.setState({loading: false});
            });
    }

    toClipboard(clipboardText) {
        try {
            this.setState({clipboardText: clipboardText}, () => {
                document.execCommand("copy");
            });
        } catch (err) {
            console.error(err);
        }
    }

    render() {
        const isDeposit = this.props.action === "deposit";
        let emptyRow = <LoadingIndicator />;
        // console.log(this.props.account , this.props.issuer_account , this.props.receive_asset)
        if (this.state.loading) {
            return emptyRow;
        }

        // let account_balances_object = this.props.account.get("balances");

        const {gateFee} = this.props;

        let withdraw_modal_id = "withdraw_bdx_modal";

        let deposit_address_fragment = this.state.clipboardText;
        let deposit_memo = null;

        let clipboardText = "";
        let memoText;
        if (this.props.deposit_account) {
            clipboardText =
                this.props.receive_coin_type +
                ":" +
                this.props.account.get("name");
            deposit_memo = <span>{clipboardText}</span>;
            var withdraw_memo_prefix = this.props.deposit_coin_type + ":";
        }

        let indicatorButtonAddr = false;

        if (isDeposit) {
            return (
                <div className="Blocktrades__gateway grid-block no-padding no-margin">
                    <div className="small-12 medium-5">
                        <Translate
                            component="h4"
                            content="gateway.deposit_summary"
                        />
                        <div className="small-12 medium-10">
                            <table className="table">
                                <tbody>
                                    <tr>
                                        <Translate
                                            component="td"
                                            content="gateway.asset_to_deposit"
                                        />
                                        <td
                                            style={{
                                                fontWeight: "bold",
                                                color: "#049cce",
                                                textAlign: "right"
                                            }}
                                        >
                                            BDX
                                        </td>
                                    </tr>
                                    <tr>
                                        <Translate
                                            component="td"
                                            content="gateway.asset_to_receive"
                                        />
                                        <td
                                            style={{
                                                fontWeight: "bold",
                                                color: "#049cce",
                                                textAlign: "right"
                                            }}
                                        >
                                            <span className="asset-prefix-replaced">
                                                beldex.
                                            </span>
                                            <span>BDX</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <Translate
                                            component="td"
                                            content="gateway.intermediate"
                                        />
                                        <td
                                            style={{
                                                fontWeight: "bold",
                                                color: "#049cce",
                                                textAlign: "right"
                                            }}
                                        >
                                            bdx
                                        </td>
                                    </tr>
                                    <tr>
                                        <Translate
                                            component="td"
                                            content="gateway.your_account"
                                        />
                                        <td
                                            style={{
                                                fontWeight: "bold",
                                                color: "#049cce",
                                                textAlign: "right"
                                            }}
                                        >
                                            <LinkToAccountById
                                                account={this.props.account.get(
                                                    "id"
                                                )}
                                            />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <Translate content="gateway.balance" />:
                                        </td>
                                        <td
                                            style={{
                                                fontWeight: "bold",
                                                color: "#049cce",
                                                textAlign: "right"
                                            }}
                                        >
                                            0&nbsp;<span className="asset-prefix-replaced">
                                                beldex.
                                            </span>
                                            <span>BDX</span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="small-12 medium-7">
                        <Translate
                            component="h4"
                            content="gateway.deposit_inst"
                        />
                        <label className="left-label">
                            <Translate
                                content="gateway.deposit_to"
                                asset={this.props.deposit_asset}
                            />:
                        </label>
                        {/* <label className="fz_12 left-label"><Translate content="gateway.deposit_notice_delay" /></label> */}
                        <div>
                            {this.state.clipboardText}
                            <div />
                            <div
                                className="button-group"
                                style={{paddingTop: 10}}
                            >
                                {deposit_address_fragment ? (
                                    <div
                                        className="button"
                                        onClick={this.toClipboard.bind(
                                            this,
                                            this.state.clipboardText
                                        )}
                                    >
                                        <Translate content="gateway.copy_address" />
                                    </div>
                                ) : null}
                                {memoText ? (
                                    <div
                                        className="button"
                                        onClick={this.toClipboard.bind(
                                            this,
                                            memoText
                                        )}
                                    >
                                        <Translate content="gateway.copy_memo" />
                                    </div>
                                ) : null}
                                {/* <button
                                    className={"button spinner-button-circle"}
                                >
                                    {indicatorButtonAddr ? (
                                        <LoadingIndicator type="circle" />
                                    ) : null}
                                    <Translate content="gateway.generate_new" />
                                </button> */}
                            </div>
                            {/* <Translate className="has-error fz_14" component="p" content="gateway.min_deposit_warning_amount" minDeposit={this.props.gateFee * 2} coin={this.props.deposit_asset} />
                            <Translate className="has-error fz_14" component="p" content="gateway.min_deposit_warning_asset" minDeposit={this.props.gateFee * 2} coin={this.props.deposit_asset} /> */}
                        </div>
                    </div>
                </div>
            );
        } else {
            return (
                <div className="Blocktrades__gateway grid-block no-padding no-margin">
                    <div className="small-12 medium-5">
                        <Translate
                            component="h4"
                            content="gateway.withdraw_summary"
                        />
                        <div className="small-12 medium-10">
                            <table className="table">
                                <tbody>
                                    <tr>
                                        <Translate
                                            component="td"
                                            content="gateway.asset_to_withdraw"
                                        />
                                        <td
                                            style={{
                                                fontWeight: "bold",
                                                color: "#049cce",
                                                textAlign: "right"
                                            }}
                                        >
                                            <span className="asset-prefix-replaced">
                                                beldex.
                                            </span>
                                            <span>BDX</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <Translate
                                            component="td"
                                            content="gateway.asset_to_receive"
                                        />
                                        <td
                                            style={{
                                                fontWeight: "bold",
                                                color: "#049cce",
                                                textAlign: "right"
                                            }}
                                        >
                                            BDX
                                        </td>
                                    </tr>
                                    <tr>
                                        <Translate
                                            component="td"
                                            content="gateway.intermediate"
                                        />
                                        <td
                                            style={{
                                                fontWeight: "bold",
                                                color: "#049cce",
                                                textAlign: "right"
                                            }}
                                        >
                                            bdx
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <Translate content="gateway.balance" />:
                                        </td>
                                        <td
                                            style={{
                                                fontWeight: "bold",
                                                color: "#049cce",
                                                textAlign: "right"
                                            }}
                                        >
                                            0&nbsp;<span className="asset-prefix-replaced">
                                                beldex.
                                            </span>
                                            <span>BDX</span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        {/*<p>When you withdraw {this.props.receive_asset.get("symbol")}, you will receive {this.props.deposit_asset} at a 1:1 ratio (minus fees).</p>*/}
                    </div>
                    <div className="small-12 medium-7">
                        <Translate
                            component="h4"
                            content="gateway.withdraw_inst"
                        />
                        <label className="left-label">
                            <Translate content="gateway.withdraw_to" />:
                        </label>
                        <div className="button-group" style={{paddingTop: 20}}>
                            <button
                                className="button success"
                                style={{fontSize: "1.3rem"}}
                                onClick={this.onWithdraw.bind(this)}
                            >
                                <Translate content="gateway.withdraw_now" />{" "}
                            </button>
                        </div>
                    </div>
                    <BaseModal id={withdraw_modal_id} overlay={true}>
                        <br />
                        <div className="grid-block vertical">
                            <WithdrawModalBlocktrades
                                account={this.props.account.get("name")}
                                url={this.state.url}
                                output_coin_name={this.props.deposit_asset_name}
                                gateFee={gateFee}
                                output_coin_symbol={this.props.deposit_asset}
                                output_coin_type={this.props.deposit_coin_type}
                                output_wallet_type={
                                    this.props.deposit_wallet_type
                                }
                                output_supports_memos={
                                    this.props.supports_output_memos
                                }
                                modal_id={withdraw_modal_id}
                                // balance={1000}
                                accountId={this.props.account.get("id")}
                            />
                        </div>
                    </BaseModal>
                </div>
            );
        }
    }
}

export default BindToChainState(BlockTradesGatewayDepositRequest, {
    keep_updating: true
});
