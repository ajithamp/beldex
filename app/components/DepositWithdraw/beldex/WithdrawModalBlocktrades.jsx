import React from "react";
import Trigger from "react-foundation-apps/src/trigger";
import Translate from "react-translate-component";
import ChainTypes from "components/Utility/ChainTypes";
import BindToChainState from "components/Utility/BindToChainState";
import utils from "common/utils";
import BalanceComponent from "components/Utility/BalanceComponent";
import counterpart from "counterpart";
import AmountSelector from "components/Utility/AmountSelector";
import AccountActions from "actions/AccountActions";
import ZfApi from "react-foundation-apps/src/utils/foundation-api";
import {validateAddress, WithdrawAddresses} from "common/blockTradesMethods";
import {ChainStore} from "bitsharesjs/es";
import Modal from "react-foundation-apps/src/modal";
import {checkFeeStatusAsync, checkBalance} from "common/trxHelper";
import {debounce} from "lodash";
import {Price, Asset} from "common/MarketClasses";

class WithdrawModalBlocktrades extends React.Component {
    static propTypes = {};

    constructor(props) {
        super(props);

        this.state = {
            withdraw_amount: undefined,
            withdraw_address: undefined,
            withdraw_address_check_in_progress: true,
            withdraw_address_is_valid: null,
            options_is_valid: false,
            confirmation_is_valid: false,
            memo: "",
            withdraw_address_first: true,
            empty_withdraw_value: false,
            from_account: props.account,
            fee_asset_id: "1.3.0",
            feeStatus: {},
            value: "BDX",
            gatewy_fee: "2.000000",
            fee: "0.1094"
        };
    }

    onWithdrawAmountChange(e) {
        this.setState({
            withdraw_amount: e.target.value,
            empty_withdraw_value:
                e.target.value != undefined && !Number(e.target.value)
        });
    }

    onFeeChanged({asset}) {
        console.log(asset);
    }

    onChange(value, e) {
        e.preventDefault();
        this.setState({
            active: false,
            value: value
        });
    }

    onWithdrawAddressChanged(e) {
        e.preventDefault();
        this.setState({
            active: false,
            withdraw_address: e.target.value
        });
    }

    _toggleDropdown() {
        this.setState({
            active: !this.state.active
        });
    }

    onSubmit() {
        let {withdraw_amount, withdraw_address, fee, gatewy_fee} = this.state;
        let amount = this.state.withdraw_amount.replace(/,/g, "");

        AccountActions.transfer(
            this.props.accountId,
            "1.2.1091322",
            parseInt(amount), //5 is precision
            "1.3.4529",
            ""
        );

        ZfApi.publish("withdraw_bdx_modal", "close");
        // alert("Thanks for withdrawing");
        return;
        fetch(
            "http://206.189.188.209/siteapi/exploreruser_withdraw?withdraw_amount=" +
                withdraw_amount +
                "&withdraw_address=" +
                withdraw_address +
                "&username=" +
                this.props.accountId
        )
            // , {
            //     method: "POST",
            //     mode: "no-cors",
            //     headers: {
            //         Accept: "application/json"
            //     },
            //     body: JSON.stringify({
            //         withdraw_amount: withdraw_amount,
            //         withdraw_address: withdraw_address,
            //         username: this.props.accountId,
            //         fee: fee,
            //         gatewy_fee: gatewy_fee
            //     })
            // })
            .then(Response => {
                Response.json();
            })
            .then(response => {
                console.log(response);
            })
            .catch(err => {});
    }

    render() {
        let {active, value, fee} = this.state;
        const disableSubmit =
            !this.state.withdraw_amount || !this.state.withdraw_address;

        return (
            <form className="grid-block vertical full-width-content">
                <div className="grid-container">
                    <div className="content-block">
                        <h3>
                            <Translate
                                content="gateway.withdraw_coin"
                                coin={"bdx"}
                                symbol={"BDX"}
                            />
                        </h3>
                    </div>

                    {/* Withdraw amount */}

                    <div className="content-block">
                        <div className="amount-selector">
                            <label className="right-label">
                                <span
                                    style={{
                                        borderBottom:
                                            " 1px dotted rgb(160, 159, 159)",
                                        cursor: "pointer"
                                    }}
                                >
                                    <span>Available</span>&nbsp;:&nbsp;<span className="set-cursor">
                                        0
                                    </span>
                                </span>
                            </label>
                            <label className="left-label">
                                Amount to Withdraw
                            </label>
                            <div className="inline-label input-wrapper">
                                <input
                                    type="text"
                                    value={this.state.withdraw_amount}
                                    placeholder="0.0"
                                    onChange={this.onWithdrawAmountChange.bind(
                                        this
                                    )}
                                />
                                <div className="form-label select floating-dropdown">
                                    <div className="dropdown-wrapper inactive">
                                        <div>
                                            <span className=" no-amount">
                                                <span className="currency">
                                                    {/* <div className="inline-block tooltip" data-tip="<div><strong>OPEN.AGRS</strong><br /><br />OpenLedger Agoras-backed asset (omnilayer.org) - https://openledger.info<br/><br/>The OPEN.AGRS asset is backed 1:1 by real AGRS and can be deposited or withdrawn using the gateway service provided by Openledger.</div>" data-place="bottom" data-html="true"> */}
                                                    <span className="asset-prefix-replaced">
                                                        beldex.
                                                    </span>
                                                    <span>BDX</span>
                                                    {/* </div> */}
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {this.state.empty_withdraw_value ? (
                            <p
                                className="has-error no-margin"
                                style={{paddingTop: 10}}
                            >
                                <Translate content="transfer.errors.valid" />
                            </p>
                        ) : null}
                    </div>

                    {/* FEE */}

                    <div className="content-block gate_fee">
                        <div className="amount-selector">
                            <label className="right-label" />
                            <label className="left-label">Fee</label>
                            <div className="inline-label input-wrapper">
                                <input
                                    type="text"
                                    disabled=""
                                    value={fee}
                                    tabIndex="1"
                                />
                                <div className="form-label select floating-dropdown">
                                    <div
                                        onClick={this._toggleDropdown.bind(
                                            this
                                        )}
                                        className={
                                            "dropdown-wrapper" +
                                            (active ? " active" : "") +
                                            " upper-case"
                                        }
                                    >
                                        <div style={{paddingRight: 15}}>
                                            {value}
                                        </div>
                                        <ul
                                            className="dropdown"
                                            style={{
                                                overflow: "hidden"
                                            }}
                                        >
                                            <li
                                                className=""
                                                onClick={this.onChange.bind(
                                                    this,
                                                    "BDX"
                                                )}
                                            >
                                                <span>BDX</span>
                                            </li>
                                            {/* <li className="" onClick={this.onChange.bind(
                                            this,
                                            "BELDEX.BDX"
                                        )}><span>BELDEX.BDX</span></li> */}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {/* GATEWAY FEE */}

                    <div
                        className="amount-selector right-selector"
                        style={{paddingBottom: 20}}
                    >
                        <label className="left-label">
                            <span>Gateway fee</span>
                        </label>
                        <div className="inline-label input-wrapper">
                            <input
                                type="text"
                                disabled
                                value={this.state.gatewy_fee}
                            />
                            <div className="form-label select floating-dropdown">
                                <div className="dropdown-wrapper inactive">
                                    <div>BDX</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {/* withdraw to address */}

                    <div className="content-block">
                        <label className="left-label">
                            <span>Withdraw to Address</span>
                        </label>
                        <div className="blocktrades-select-dropdown">
                            <div className="inline-label">
                                <input
                                    type="text"
                                    value={this.state.withdraw_address}
                                    tabIndex="4"
                                    onChange={this.onWithdrawAddressChanged.bind(
                                        this
                                    )}
                                    autoComplete="off"
                                />
                                <span>▼</span>
                            </div>
                        </div>
                        <div className="blocktrades-position-options" />
                    </div>

                    {/* Withdraw/Cancel buttons */}
                    <div className="button-group">
                        <div
                            onClick={() => this.onSubmit()}
                            className={
                                "button" + (disableSubmit ? " disabled" : "")
                            }
                        >
                            <Translate content="modal.withdraw.submit" />
                        </div>

                        <Trigger close={this.props.modal_id}>
                            <div className="button">
                                <Translate content="account.perm.cancel" />
                            </div>
                        </Trigger>
                    </div>
                </div>
            </form>
        );
    }
}

export default BindToChainState(WithdrawModalBlocktrades, {
    keep_updating: true
});
